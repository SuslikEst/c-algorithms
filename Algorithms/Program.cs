﻿using System;

namespace Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            // ArraysDemo();
            // MultiArrays();

            int[] unsortArr = { 1, 11, 0, 12, 5, 3, 2, -1111, 5, 5 };


            Sorting.SelectionSort(unsortArr);
            Console.WriteLine("Selection sort start");
            foreach (var el in unsortArr)
            {
                Console.WriteLine(el);
            }
            Console.WriteLine("Selection sort end");

            Sorting.BubbleSort(unsortArr);
            Console.WriteLine("Bubble sort start");
            foreach (var el in unsortArr)
            {
                Console.WriteLine(el);
            }
            Console.WriteLine("Bubble sort end");

        }

        private static void MultiArrays()
        {
            int[,] a1 = { { 1, 25, 33 }, { -2, -4, -100 } };
            for (int i = 0; i < a1.GetLength(0); i++)
            {
                for (int j = 0; j < a1.GetLength(1); j++)
                {
                    Console.WriteLine($"multi: {a1[i,j]}");
                }
                Console.WriteLine();
            }
        }

        private static void ArraysDemo()
        {
            int[] a1;
            a1 = new int[10];

            int[] a4 = { 1, 9, -2, 15, 0 };

            for(int i = 0; i < a4.Length; i++)
            {
                Console.WriteLine(a4[i]);
            }

            foreach(var el in a1)
            {
                Console.WriteLine($"a4: {el}");
            }

            Console.WriteLine($"a4: {a4}");

        }
    }
}
