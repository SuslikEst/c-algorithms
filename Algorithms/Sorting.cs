﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms
{
    public class Sorting
    {
        public static void SelectionSort(int[] arr)
        {
            for (int partIndex = arr.Length - 1; partIndex > 0; partIndex--)
            {
                int largestAt = 0;
                for (int i = 0; i <= partIndex; i++)
                {
                    if (arr[i] > arr[largestAt])
                    {
                        largestAt = i;
                    }
                }
                Swap(arr, largestAt, partIndex);
            }
        }
        public static void BubbleSort(int[] arr)
        {
            for(int partIndex = arr.Length - 1; partIndex > 0; partIndex--)
            {
                for (int i = 0; i < partIndex; i++)
                {
                    if(arr[i] > arr[i + 1])
                    {
                        Swap(arr, i, i + 1);
                    }
                }
            }
        }
        private static void Swap(int[] arr, int i, int j)
        {
            if (i == j) return;
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }
}
